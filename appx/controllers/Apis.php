<?php
require APPPATH . '/libraries/REST_controller.php';
use Restserver\Libraries\REST_controller;


class Apis extends REST_controller{

  public function __construct()
  {
    parent::__construct();

    $this->load->model('Database_Model');
  }

  //Exemplo para retornar todos os resgistros, alterar so o nome
  /*function teste_get()
  {
    $result = $this->Database_Model->Select('pet',null,null,null);

    if($result){
      $this->response($result, 200); 
    } 
    else{
      $this->response("Nenhum Produto Cadastrado!", 404);
    }
  }*/
   
  //Exemplo para retornar um arquivo em especifico pelo Id
  /*
  function buscarProdutoId_get(){

    $produtoId  = $this->get('produtoId');

    if(!$produtoId){

      $this->response("produtoId não informado", 400);

      exit;
    }

    $result = $this->Produtos_Model->getprodutoporid($produtoId);

    if($result){

      $this->response($result, 200); 

      exit;
    } 
    else if($result == 0){

      $this->response("Produto não encontrado");

    }
    else{

     $this->response("Produto não encontrado!", 404);

     exit;
   }
 } */

//Exemplo para se fazer um insert via api
/*
function adicionarProduto_post(){

 $produtoNome = $this->post('produtoNome');

 $produtoEan     = $this->post('produtoEan');

 $produtoNCM    = $this->post('produtoNCM');

 $produtoCest  = $this->post('produtoCest');

 $produtoImagem  = $this->post('produtoImagem');

 $categoriaId      = $this->post('categoriaId');

 $segmentoId  = $this->post('segmentoId');

 $produtoDescricaoComp  = $this->post('produtoDescricaoComp');

 $produtoPesoBruto     = $this->post('produtoPesoBruto');

 $produtoUnidade  = $this->post('produtoUnidade');

 if (!$produtoNome || empty($produtoNome)) {
   $this->response("produtoNome não informado ou campo vazio!", 400);
 }
 elseif (!$produtoEan || empty($produtoEan)) {
   $this->response("produtoEan não informado ou campo vazio!", 400);
 }
 elseif (!$categoriaId || empty($categoriaId)) {
  $this->response("categoriaId não informado ou campo vazio!", 400);
}
elseif (!$segmentoId || empty($segmentoId)) {
 $this->response("segmentoId não informado ou campo vazio!", 400);
}
elseif ($this->Produtos_Model->verificarCampo("categoriaId",$categoriaId,"categorias")==0) {
  $this->response("categoriaId não cadastrada!", 400);
}
elseif ($this->Produtos_Model->verificarCampo("segmentoId",$segmentoId,"segmentos")==0) {
  $this->response("segmentoId não cadastrado!", 400);
}
elseif ($this->Produtos_Model->verificarCampo("produtoEan",$produtoEan,"produtos")) {
  $this->response("produtoEan já cadastrado!", 400);
}
else{
  if (!$produtoImagem) {
   $produtoImagem = null;
 }

 if (!$produtoDescricaoComp) {
   $produtoDescricaoComp = null;
 }
 if (!$produtoNCM) {
   $produtoNCM = null;
 }
 if (!$produtoCest) {
   $produtoCest = null;
 }
 if (!$produtoPesoBruto) {
   $produtoPesoBruto = null;
 }
 if (!$produtoUnidade) {
   $produtoUnidade = null;
 }

 $result = $this->Produtos_Model->addproduto(array("produtoNome"=>$produtoNome, "produtoEan"=>$produtoEan, "produtoNCM"=>$produtoNCM, "produtoCest"=>$produtoCest, "produtoImagem"=>$produtoImagem, "categoriaId"=>$categoriaId, "segmentoId"=>$segmentoId, "produtoDescricaoComp"=>$produtoDescricaoComp, "produtoPesoBruto"=>$produtoPesoBruto, "produtoUnidade"=>$produtoUnidade));

 if($result === 0){

  $this->response("Erro ao adicionar produto! Tente Novamente.", 404);

}else{

  $this->response("success", 200);  

}

}

}*/

//Exemplo para se fazer um update via api
/*
function atualizarProduto_put(){

  $produtoNome = $this->put('produtoNome');

  $produtoEan     = $this->put('produtoEan');

  $produtoNCM    = $this->put('produtoNCM');

  $produtoCest  = $this->put('produtoCest');

  $produtoImagem  = $this->put('produtoImagem');

  $categoriaId      = $this->put('categoriaId');

  $segmentoId  = $this->put('segmentoId');

  $produtoDescricaoComp  = $this->put('produtoDescricaoComp');

  $produtoPesoBruto     = $this->put('produtoPesoBruto');

  $produtoUnidade  = $this->put('produtoUnidade');


  if ($produtoNome) {
    if (empty($produtoNome)) {
     $this->response("produtoNome não pode ser vazio!", 400);
   }

 }
 elseif (!$produtoEan || empty($produtoEan)) {
   $this->response("produtoEan não informado ou campo vazio!", 400);
 }
 elseif ($this->Produtos_Model->verificarCampo("produtoEan",$produtoEan,"produtos")==0) {
  $this->response("produtoEan não encontrado!", 400);
}
elseif ($categoriaId) {
  if ($this->Produtos_Model->verificarCampo("categoriaId",$categoriaId,"categorias")==0) {
    $this->response("categoriaId não cadastrada!", 400);
  }
}
elseif ($segmentoId) {
  if ($this->Produtos_Model->verificarCampo("segmentoId",$segmentoId,"segmentos")==0) {
    $this->response("segmentoId não cadastrado!", 400);
  }
}
else{
  $data = array("produtoNome"=>$produtoNome, "produtoNCM"=>$produtoNCM, "produtoCest"=>$produtoCest, "categoriaId"=>$categoriaId, "segmentoId"=>$segmentoId, "produtoPesoBruto"=>$produtoPesoBruto, "produtoUnidade"=>$produtoUnidade, "produtoImagem"=>$produtoImagem,"produtoDescricaoComp"=>$produtoDescricaoComp);
  if (!$produtoImagem) {
   unset($data["produtoImagem"]);
 }

 if (!$produtoDescricaoComp) {
  unset($data["produtoDescricaoComp"]);
}
if (!$produtoNCM) {
 unset($data["produtoNCM"]);
}
if (!$produtoCest) {
 unset($data["produtoCest"]);
}
if (!$produtoPesoBruto) {
 unset($data["produtoPesoBruto"]);
}
if (!$produtoUnidade) {
 unset($data["produtoUnidade"]);
}
if (!$categoriaId) {
 unset($data["categoriaId"]);
}
if (!$segmentoId) {
 unset($data["segmentoId"]);
}
if (!$produtoNome) {
 unset($data["produtoNome"]);
}
$result = $this->Produtos_Model->updateproduto($produtoEan, $data);

if($result === 0){

  $this->response("Erro ao atualizar Produto!", 404);

}else{

  $this->response("success", 200);  

}

}

}
*/
//Exemplo para se fazer um delete via api
/*
function deletarProduto_delete()
{

  $produtoEan = $this->delete('produtoEan');

  if(!$produtoEan){

    $this->response("produtoEan não informado!", 400);

  }

  if ($this->Produtos_Model->verificarCampo("produtoEan",$produtoEan,"produtos")==0) {
    $this->response("produtoEan não encontrado!", 400);
  }


  if($this->Produtos_Model->deleteproduto($produtoEan))
  {

    $this->response("Success", 200);

  } 
  else
  {

    $this->response("Erro ao apagar produto!", 404);

  }

}

*/


}