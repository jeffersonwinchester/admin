<?php
class Database_Model extends CI_Model {
 
  public function __construct(){
    
    $this->load->database();
    
  }

  /**************************Utilizando Select*******************************/
  /*
  Parametros:
  $Tabela = Recebe uma string com o nome da tabela em que deseja fazer o select
  Ex: Cliente

  $Campos = Recebe um array com os campos em que deseja que a consulta retorne
  Ex: array("cliente_nome", "cliente_telefone", "cliente_idade");

  $Condicoes = Recebe um array com as condicoes da consulta
  Ex: array(
			"cliente_id" => "1",
      "cliente_nome" => "Jefferson",
		);

  $Order = Recebe um array com a ordenação que deseja os campos
  Ex: array("cliente_id asc");

  Obs: Todos os parametros sao opcionais, exceto o da tabela
  */
  public function Select($Tabela, $Campos, $Condicoes, $Order){  
    if (!empty($Campos)) {
      $this->db->select($this->GetCampos($Campos));
    }
    if (!empty($Condicoes)) {
      foreach ($Condicoes as $Campo => $Valor) {
        $this->db->where($Campo, $Valor);
      }
    }
    if (!empty($Order)) {
      $this->db->order_by($this->GetCampos($Order));
    }
    $this->db->from($Tabela);
    $Result = $this->db->get();
    $Error = $this->db->error()['message'];
      
    if(!empty($Error)) {
      throw new Exception('Codigo do erro: '.$this->db->error()['code'].' Erro ao fazer select: '.$Error);
    }
    else
    {
      if ($Result->num_rows() > 0) {
        return $Result->result_array();
      } else {
        return NULL;
      }
    }
 }

/**************************Utilizando Select Join*******************************/
  /*
  Parametros:
  $Tabela = Recebe uma string com o nome da tabela em que deseja fazer o select
  Ex: Cliente

  $Campos = Recebe um array com os campos em que deseja que a consulta retorne
  Ex: array("cliente_nome", "cliente_telefone", "cliente_idade");

  $Condicoes = Recebe um array com as condicoes da consulta
  Ex: array(
			"cliente_id" => "1",
      "cliente_nome" => "Jefferson",
		);

  $Order = Recebe um array com a ordenação que deseja os campos
  Ex: array("cliente_id asc");

  $Joins = Recebe um array com a relação das tabelas
  Ex: array(
			"endereco" => "endereco.cliente_id = endereco.cliente_id",
		);
  Obs: Todos os parametros sao opcionais, exceto o da tabela

  $Tipo = Recebe o tipo da relação, se passado null é entendido como join
  Ex: Join, Left
  */
 public function SelectJoin($Tabela, $Campos, $Condicoes, $Order, $Joins, $Tipo){  
  if (!empty($Campos)) {
    $this->db->select($this->GetCampos($Campos));
  }
  if (!empty($Condicoes)) {
    foreach ($Condicoes as $Campo => $Valor) {
      $this->db->where($Campo, $Valor);
    }
  }
  if (!empty($Order)) {
    $this->db->order_by($this->GetCampos($Order));
  }
  $this->db->from($Tabela);
  if (!empty($Joins)) {
    if (empty($Tipo)) {
      $Tipo = 'Join';
    }
    foreach ($Joins as $Tabela => $Relacao) {
      $this->db->join($Tabela, $Relacao, $Tipo);
    }
  }
  
  $Result = $this->db->get();
  $Error = $this->db->error()['message'];
    
  if(!empty($Error)) {
    throw new Exception('Codigo do erro: '.$this->db->error()['code'].' Erro ao fazer select join: '.$Error);
  }
  else
  {
    if ($Result->num_rows() > 0) {
      return $Result->result_array();
    } else {
      return NULL;
    }
  }
}

/**************************Utilizando Insert*******************************/
  /*
  Parametros:
  $Tabela = Recebe uma string com o nome da tabela em que deseja fazer o Insert
  Ex: Cliente

  $Dados = Recebe um array com os dados a serem inseridos
  Ex: array(
			"cliente_id" => "1",
      "cliente_nome" => "Jefferson",
		);
  */

public function Insert($Tabela, $Dados){
  $this->db->insert($Tabela, $Dados);
  $Error = $this->db->error()['message']; 
  if(!empty($Error)) {
    throw new Exception('Codigo do erro: '.$this->db->error()['code'].' Erro ao fazer insert: '.$Error);
  }
}

/***************************Utilizando Delete*********************************/
  /*
  Parametros:
  $Tabela = Recebe uma string com o nome da tabela em que deseja fazer o Delete
  Ex: Cliente

  $Condicoes = Recebe um array com as condicoes para apagar
  Ex: array(
			"cliente_id" => "1",
		);
  */
public function Delete($Tabela, $Condicoes){
  if (!empty($Condicoes)) {
    foreach ($Condicoes as $Campo => $Valor) {
      $this->db->where($Campo, $Valor);
    }
  }
  $this->db->delete($Tabela);
  $Error = $this->db->error()['message']; 
  if(!empty($Error)) {
    throw new Exception('Codigo do erro: '.$this->db->error()['code'].' Erro ao fazer delete: '.$Error);
  }
}

/********************************************Utilizando Update**********************************/
  /*
  Parametros:
  $Tabela = Recebe uma string com o nome da tabela em que deseja fazer o Update
  Ex: Cliente

  $Dados = Recebe um array com os dados a serem atualizados
  Ex: array(
			"cliente_id" => "1",
      "cliente_nome" => "Jefferson",
		);

  $Condicoes = Recebe um array com as condicoes para atualizar
  Ex: array(
			"cliente_id" => "1",
		);
  */
public function Update($Tabela, $Dados, $Condicoes){
  if (!empty($Condicoes)) {
    foreach ($Condicoes as $Campo => $Valor) {
      $this->db->where($Campo, $Valor);
    }
  }
  $this->db->update($Tabela, $Dados);
  $Error = $this->db->error()['message']; 
  if(!empty($Error)) {
    throw new Exception('Codigo do erro: '.$this->db->error()['code'].' Erro ao fazer update: '.$Error);
  }
}

/****************************************Utilizando Sum**************************/
/*
  Parametros:
  $Tabela = Recebe uma string com o nome da tabela em que deseja fazer a soma
  Ex: Cliente

  $Campo = Recebe uma string com o nome do campo que deseja fazer a soma
  Ex: cliente_id

  $Condicoes = Recebe um array com as condicoes para somar
  Ex: array(
			"cliente_id" => "1",
		);
  */
public function Sum($Tabela, $Campo, $Condicoes)
{
  if (!empty($Condicoes)) {
    foreach ($Condicoes as $Campo => $Valor) {
      $this->db->where($Campo, $Valor);
    }
  }
	$this->db->select('sum('.$Campo.') as soma');
	$this->db->from($Tabela);
	$result = $this->db->get();
  $Error = $this->db->error()['message']; 
  if(!empty($Error)) {
    throw new Exception('Codigo do erro: '.$this->db->error()['code'].' Erro ao fazer sum: '.$Error);
  }
  else
  {
    if (!empty($Result)) {
      return $result->row()->soma;
    } else {
      return 0;
    }
  }
}

/****************************************Utilizando Count**************************/
/*
  Parametros:
  $Tabela = Recebe uma string com o nome da tabela em que deseja fazer a contagem
  Ex: Cliente

  $Campo = Recebe uma string com o nome do campo que deseja fazer a contagem
  Ex: cliente_id

  $Condicoes = Recebe um array com as condicoes para contar
  Ex: array(
			"cliente_id" => "1",
		);
  */
  public function Count($Tabela, $Campo, $Condicoes)
  {
    if (!empty($Condicoes)) {
      foreach ($Condicoes as $Campo => $Valor) {
        $this->db->where($Campo, $Valor);
      }
    }
    $this->db->select('count('.$Campo.') as soma');
    $this->db->from($Tabela);
    $result = $this->db->get();
    $Error = $this->db->error()['message']; 
    if(!empty($Error)) {
      throw new Exception('Codigo do erro: '.$this->db->error()['code'].' Erro ao fazer count: '.$Error);
    }
    else
    {
      if (!empty($Result)) {
        return $result->row()->soma;
      } else {
        return 0;
      }
    }
  }

/*************************************Verifica se existe o registro no Banco********************/
/*
  Parametros:
  $Tabela = Recebe uma string com o nome da tabela ao qual deseja verificar se o registro existe
  Ex: Cliente

  $Condicoes = Recebe um array com as condicoes para verificar
  Ex: array(
			"cliente_id" => "1",
		);
  */
public function RegistroExiste($Tabela, $Condicoes)
{
  if (!empty($Condicoes)) {
    foreach ($Condicoes as $Campo => $Valor) {
      $this->db->where($Campo, $Valor);
    }
  }
  $this->db->from($Tabela);
  $Result = $this->db->get();
  $Error = $this->db->error()['message']; 
  if(!empty($Error)) {
    throw new Exception('Codigo do erro: '.$this->db->error()['code'].' Erro ao verificar se registro existe: '.$Error);
  }
  else
  {
    return $Result->num_rows() > 0;
  }
}

/***************************************************************************************************/
private function GetCampos($Campos)
{
  $Fields = '';
  foreach ($Campos as $Field) {
    $Fields .= $Field . ',';
  }
  return rtrim($Fields, ",");
}

}