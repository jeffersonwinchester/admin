</div>
  </div>

  <!-- Scroll to top -->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <script src="<?php echo base_url(); ?>public/js/jquery.min.js?v=<?php echo Versao; ?>"></script>
  <script src="<?php echo base_url(); ?>public/js/bootstrap.bundle.min.js?v=<?php echo Versao; ?>"></script>
  <script src="<?php echo base_url(); ?>public/js/jquery.easing.min.js?v=<?php echo Versao; ?>"></script>
  <script src="<?php echo base_url(); ?>public/js/ruang-admin.min.js?v=<?php echo Versao; ?>"></script>
  <script src="<?php echo base_url(); ?>public/js/Chart.min.js?v=<?php echo Versao; ?>"></script>
  <script src="<?php echo base_url(); ?>public/js/chart-area-demo.js?v=<?php echo Versao; ?>"></script> 
  <!-- Base -->
  <script src="<?php echo base_url(); ?>appx/protected/base.js?v=<?php echo versao; ?>"></script>

  <!-- Alerts -->
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> 
</body>

</html>