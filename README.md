# codeigniter/framework

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)

## About <a name = "about"></a>

Projeto de um aplicativo para restaurante/lanchonete da 4A.

## Getting Started <a name = "getting_started"></a>

#TODO

### Prerequisites

Possuir o docker instalado na maquina ou algum webserver como o apache(XAMPP, LAMP) e um banco de dados MySql.

### Installing

#INSTALAÇÃO COM DOCKER

Para instalar as depedências desse projeto utilizando o docker, simplismente utilize o comando no terminal:
```
    docker-compose up -d
```

O projeto vai inicial na porta: http://localhost:8000/
Para acessar o banco de dados, entre em: http://localhost:8080/


O projeto também pode ser iniciado utilizando-se o xampp.

## Usage <a name = "usage"></a>

#TODO
